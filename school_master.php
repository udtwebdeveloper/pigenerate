<?php
/**
 * Created by PhpStorm.
 * User: Aakash
 * Date: 04-10-2017
 * Time: 12:20
 */
session_start();
include "connection.php";
require 'FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
$school_location = '';

if (isset($_GET['logout'])) {
    $_SESSION['user'] = '';
    $_SESSION = array();
}
if (isset($_GET['school_code_ajax'])) {
    $last_month_start = date('Y-m-01', strtotime('last month'));
    $last_month_end = date('Y-m-t', strtotime('last month'));
    $query = "SELECT total_sent_messages as msg_sent FROM users_count UC WHERE school_code ='" . $_GET['school_code_ajax'] . "' AND date(UC.created_date)>='" . $last_month_start . "' AND date(UC.created_date)<='" . $last_month_end . "'  order by date(created_date) DESC LIMIT  1";
    $result = mysqli_query($connect, $query);
    $message_sent = '';
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $message_sent = $row['msg_sent'];
        }
        $message_sent .= " Sms Sent.";
    } else {
        $message_sent = "0 Sms sent OR school code does not exists";
    }
    echo json_encode($message_sent);
    exit;
}
if (isset($_GET['school_state_ajax'])) {
    $school_state = $_GET['school_state_ajax'];
    $query = "select city_name from states_cities where   LOWER(state) LIKE '%" . strtolower($_GET['school_state_ajax']) . "%'";
    $result = mysqli_query($connect, $query);
    $cities = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $cities[$row['city_name']] = $row['city_name'];
        }

    }

    echo json_encode($cities);
    exit;
}
$is_agreement = '';
$status = '';
$package_type = '';
if (isset($_SESSION['user']) && $_SESSION['user'] == 'admin') {
    $query = "select team_name,id from team_m where status=1";
    //echo $query;die;
    $result = mysqli_query($connect, $query);
    $i = 1;
    $team = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $team[$row['id']] = $row['team_name'];
        }
    }
    /*$query = //"select tanent_id from master_dbs where status=1";
    //echo $query;die;
    $result = mysqli_query($connect, $query);
    $i=1;
    $school_codes=array();
    if($result->num_rows > 0){
        while ($row = mysqli_fetch_array($result)) {
            $school_codes[$row['tanent_id']]=$row['tanent_id'];
        }
    }*/

    $states_name = array();
    $locations = array();
    $query = "select concat(city_name,'-',state) as location,city_id as id from states_cities";
    //echo $query;die;
    $result = mysqli_query($connect, $query);
    $i = 1;
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $locations[$row['location']] = $row['location'];
        }
    }
    $query = "select state,city_id as id from states_cities GROUP by state";
    //echo $query;die;
    $result = mysqli_query($connect, $query);
    $i = 1;
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $states_name[$row['state']] = $row['state'];
        }
    }

    if (isset($_POST['school_name'])) {
        $post_data = $_POST;
        $sch_id = !empty($post_data['sid']) ? $post_data['sid'] : '';
        $school_name = $post_data['school_name'];
        $school_code = $post_data['school_code'];
        $school_state = $post_data['school_state'];
        $school_location = $post_data['school_location'];
        $team_id = $post_data['team_id'];
        $package_type = $post_data['package_type'];
        $free_sms_count = $post_data['free_sms_count'];
        $sms_charging_rate = $post_data['sms_charging_rate'];
        $billing_start_date = !empty($post_data['billing_start_date']) ? date('Y-m-d', strtotime($post_data['billing_start_date'])) : '';
        $payment_rate = $post_data['payment_rate'];
        $agreement_date = !empty($post_data['agreement_date']) ? date('Y-m-d', strtotime($post_data['agreement_date'])) : '';
        if (!empty($post_data['is_agreement']))
            $is_agreement = $post_data['is_agreement'] == 'on' ? 1 : 0;
        else
            $is_agreement = 0;
        $security = $post_data['security_amt'];
        if (!empty($post_data['status']))
            $status = $post_data['status'] == 'on' ? 1 : 0;
        else
            $status = 0;
        $created_date = date('Y-m-d h:s:i');
        $action = !empty($post_data['action']) ? $post_data['action'] : '';

        if ($action == '') {
            $query_school = "select * from school_m S  where S.school_code='" . $school_code . "'";
            $result_school = mysqli_query($connect, $query_school);
            if ($result_school->num_rows <= 0) {
                $sql_statement = "INSERT INTO school_m (school_name, school_code, school_state, school_location, team_id,package_type, free_sms_count, sms_charging_rate, bill_start_date, package_rate,agreement_date,is_agreement,security_amount,status,created_on) 
          VALUES ('" . $school_name . "', '" . $school_code . "','" . $school_state . "', '" . $school_location . "', '" . $team_id . "','" . $package_type . "', '" . $free_sms_count . "', '" . $sms_charging_rate . "'
          ,'" . $billing_start_date . "','" . $payment_rate . "','" . $agreement_date . "','" . $is_agreement . "','" . $security . "','" . $status . "','" . $created_date . "')";
                $flag = mysqli_query($connect, $sql_statement);
                if ($flag) {
                    $msg->success('School Has Been saved Successfully');
                    //header("Location: school_master.php"); /* Redirect browser */
                } else {
                    $msg->error('There is an error during save');
                }
            }else {
            $msg->error('School already created for this code');
        }

    } else {

        $sql_statement = "UPDATE school_m SET school_name='" . $school_name . "', school_code= '" . $school_code . "', school_state='" . $school_state . "', school_location ='" . $school_location . "', team_id='" . $team_id . "',package_type='" . $package_type . "', free_sms_count='" . $free_sms_count . "', sms_charging_rate='" . $sms_charging_rate . "'
            , bill_start_date='" . $billing_start_date . "', package_rate='" . $payment_rate . "',agreement_date='" . $agreement_date . "',
            is_agreement='" . $is_agreement . "',security_amount='" . $security . "',status='" . $status . "',created_on='" . $created_date . "' WHERE id='" . $sch_id . "'";
        $flag = mysqli_query($connect, $sql_statement);
        if ($flag) {
            $msg->success('School Has Been updated Successfully');

        } else {
            $msg->error('There is an error during save');

        }
    }


}
if (isset($_GET['action']) && $_GET['action'] == 'edit') {
    $school_id = !empty($_GET['school_id']) ? $_GET['school_id'] : '';
    $query = "select * from school_m S,team_m T where S.team_id=T.id AND S.id=" . $school_id . "";
    $result = mysqli_query($connect, $query);
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            //print_r($row);die;
            $sid = $school_id;
            $school_name = $row['school_name'];
            $school_code = $row['school_code'];
            $school_state = $row['school_state'];
            $school_location = $row['school_location'];
            $team_id = $row['team_id'];
            $package_type = $row['package_type'];
            $free_sms_count = $row['free_sms_count'];
            $sms_charging_rate = $row['sms_charging_rate'];
            $billing_start_date = !empty($row['bill_start_date']) ? date('d-m-Y', strtotime($row['bill_start_date'])) : '';
            $payment_rate = $row['package_rate'];
            $agreement_date = !empty($row['agreement_date']) ? date('d-m-Y', strtotime($row['agreement_date'])) : '';
            $is_agreement = $row['is_agreement'];
            $security = $row['security_amount'];
            $status = !empty($row[12]) ? $row[12] : 0;
            $update = "update";
        }
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    $school_id = !empty($_GET['school_id']) ? $_GET['school_id'] : '';
    $query = "delete from school_m where  id=" . $school_id;
    $result = mysqli_query($connect, $query);
    if ($result) {
        $msg->success('School Has Been deleted Successfully');
    } else {
        $msg->error('There is an error during save');

    }
    exit;
}
?>
    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/mystyle.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    </head>
    <body>
    <?php include "header.php" ?>

    <div class="container-fluid h-100">

        <div class="row h-100">
            <?php include "left_menu.php"; ?>
            <div class="col pt-2 col-md-10">

                <h2 class="form-signin-heading">School Master</h2>
                <?php if ($msg->hasMessages($msg::SUCCESS)) { ?>
                    <div class="alert-success"><?php echo $msg->display(); ?></div>
                <?php } else { ?>
                    <div class="alert-warning">
                        <?php echo $msg->display(); ?>
                    </div>

                    <?php

                } ?>

                <form class="form-signin  mt-10" method="post">
                    <div class="col-md-12">
                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <input type="text" name="school_name" placeholder="School Name"
                                       class="form-control  float-left col-md-4"
                                       value="<?php echo isset($school_name) ? $school_name : '' ?>">

                                <input type="text" name="school_code" placeholder="School Code" id="school_code"
                                       class="form-control  float-left col-md-4 ml-5"
                                       value="<?php echo isset($school_code) ? $school_code : '' ?>">
                                <!--<select class="form-control col-md-4 float-left ml-5" name="school_code" required id="school_code" value="<?php /*echo isset($school_code)?$school_code:'' */ ?>">
                                    <option value="">School Code</option>
                                    <?php
                                /*                                    foreach ($school_codes as $key=>$t) {
                                                                        if($school_code == $t){
                                                                        */ ?>
                                            <option value="<?php /*echo $key; */ ?>" selected><?php /*echo $t; */ ?></option>
                                       <?php /*}

                                        else{
                                            */ ?>

                                            <option value="<?php /*echo $key; */ ?>"><?php /*echo $t; */ ?></option>
                                            <?php
                                /*                                        }
                                                                    } */ ?>

                                </select>-->

                                <!-- <input type="text" name="school_code" placeholder="School Code" required id="school_code"
                                        class="form-control  float-left col-md-4 ml-5">-->
                            </div>
                        </div>

                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <select class="form-control  float-left col-md-4" name="school_state" id="school_state">
                                    <option>Select State</option>
                                    <?php foreach ($states_name as $key => $t) {
                                        if (strtolower($school_state) == strtolower($t)) {
                                            ?>
                                            <option value="<?php echo $key; ?>" selected><?php echo $t; ?></option>
                                        <?php } else {
                                            ?>

                                            <option value="<?php echo $key; ?>"><?php echo $t; ?></option>
                                            <?php
                                        }
                                    } ?>

                                </select>
                                <select class="form-control  float-left col-md-4  ml-5" name="school_location"
                                        id="school_location">
                                    <option>Select City</option>

                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12 mt-5">
                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <select class="form-control  float-left col-md-4" name="team_id" required>
                                    <option value="">Team Name</option>
                                    <?php foreach ($team as $key => $t) { ?>
                                        <?php

                                        if (strtolower($team_id) == strtolower($key)) {
                                            ?>
                                            <option value="<?php echo $key; ?>" selected><?php echo $t; ?></option>
                                        <?php } else {
                                            ?>

                                            <option value="<?php echo $key; ?>"><?php echo $t; ?></option>
                                            <?php
                                        }
                                        ?>
                                    <?php } ?>

                                </select>

                                <select class="form-control  float-left col-md-4  ml-5" id="exampleSelect1"
                                        name="package_type">
                                    <option value="">Package Type</option>
                                    <option value="monthly" <?php echo $package_type == "monthly" ? "selected" : '' ?>>
                                        Monthly
                                    </option>
                                    <option value="quartely" <?php echo $package_type == "quartely" ? "selected" : '' ?>>
                                        Quarterly
                                    </option>
                                    <option value="yearly" <?php echo $package_type == "yearly" ? "selected" : '' ?>>
                                        Yearly
                                    </option>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <input type="text" name="free_sms_count" placeholder="Free Sms Count"
                                       value="<?php echo isset($free_sms_count) ? $free_sms_count : '' ?>"
                                       class="form-control  float-left col-md-4">
                                <input type="text" name="sms_charging_rate" placeholder="Sms Charging Rate"
                                       value="<?php echo isset($sms_charging_rate) ? $sms_charging_rate : '' ?>"
                                       class="form-control  float-left col-md-4 ml-5">
                            </div>
                            <div class="clearfix"></div>
                            <div id="sms_sent" class="btn-info col-md-4 mt-2" style="padding-left: 30px;"></div>

                        </div>

                    </div>
                    <div class="col-md-12 mt-5">

                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <div class="col-md-5 float-left">
                                    <label for="dtp_input2" class="control-label float-left">Billing Date</label>
                                    <div class="input-group date form_date " data-date="" data-date-format="dd-mm-yyyy"
                                         data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type="text"
                                               value="<?php echo isset($billing_start_date) ? $billing_start_date : '' ?>"
                                               readonly
                                               name="billing_start_date">
                                        <span class="input-group-addon"><span class="fa  fa-calendar"></span></span>
                                    </div>
                                    <input type="hidden" id="dtp_input2" value=""/>
                                </div>

                                <div class="col-md-5 float-left ">
                                    <label for="dtp_input2" class="control-label float-left">Billing Rate</label>

                                    <input type="text" name="payment_rate" placeholder="Bill Rate"
                                           value="<?php echo isset($payment_rate) ? $payment_rate : '' ?>"
                                           class="form-control  float-left">
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <div class="float-left col-md-5 mt-4">
                                    <label class="">Is Agreement Done?</label>
                                    <input type="checkbox"
                                           name="is_agreement" <?php echo $is_agreement == '1' ? "checked" : '' ?> >
                                </div>

                                <div class="col-md-5 float-left">
                                    <label for="dtp_input2" class="control-label float-left">Agreement Date</label>
                                    <div class="input-group date form_date " data-date="" data-date-format="dd-mm-yyyy"
                                         data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type="text"
                                               value="<?php echo isset($agreement_date) ? $agreement_date : '' ?>"
                                               readonly
                                               name="agreement_date">
                                        <span class="input-group-addon"><span class="fa  fa-calendar"></span></span>
                                    </div>
                                    <input type="hidden" id="dtp_input2" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 mt-5">
                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <div class="col-md-5 float-left ">
                                    <input type="text" name="security_amt" placeholder="Security Amount"
                                           class="form-control  float-left mt-4"
                                           value="<?php echo isset($security) ? $security : '' ?>">

                                </div>
                                <div class="col-md-5 float-left ">

                                    <label class="">Active?</label>
                                    <input type="checkbox" name="status" class=" mt-4" <?php

                                    echo $status == '1' ? "checked" : '' ?>>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6 float-left">
                            <div class="form-group">
                                <div class="col-md-5 float-left ">
                                </div>
                                <div class="col-md-5 float-left ">
                                    <input type="submit" class="form-control btn btn-info mt-4 float-left"
                                           value="Submit">
                                </div>

                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($update) ? $update : '' ?>" name="action">
                    <input type="hidden" value="<?php echo isset($sid) ? $sid : '' ?>" name="sid">
                </form>
                <?php
                $query = "select *,S.id as school_id from school_m S,team_m T where S.team_id=T.id";
                $result = mysqli_query($connect, $query);
                ?>

                <div class="col-md-10" style="margin-top:200px;">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th>S.Name</th>
                            <th>S.Code</th>
                            <th>S.State</th>
                            <th>S.City</th>
                            <th>Team</th>
                            <th>Package</th>
                            <th>Free SMS</th>
                            <th>SMS Charges</th>
                            <th>Billing Start</th>
                            <th>Bill Rate</th>
                            <th>Agg.Date</th>
                            <th>Security</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        /*                        echo $query;die;*/
                        if ($result->num_rows > 0) {
                            $data_saved = 0;

                            while ($row = mysqli_fetch_array($result)) {
                                /*   echo "<pre>";
                                   print_r($row);die;*/

                                echo "<tr>";
                                echo "<td>" . $row['school_name'] . "</td>";
                                echo "<td>" . $row['school_code'] . "</td>";
                                echo "<td>" . $row['school_state'] . "</td>";
                                echo "<td>" . $row['school_location'] . "</td>";
                                echo "<td>" . $row['team_name'] . "</td>";
                                echo "<td>" . $row['package_type'] . "</td>";
                                echo "<td>" . $row['free_sms_count'] . "</td>";
                                echo "<td>" . $row['sms_charging_rate'] . "</td>";
                                echo "<td>" . date('d-m-Y', strtotime($row['bill_start_date'])) . "</td>";
                                echo "<td>" . $row['package_rate'] . "</td>";
                                echo "<td>" . date('d-m-Y', strtotime($row['agreement_date'])) . "</td>";
                                echo "<td>" . $row['security_amount'] . "</td>";
                                echo "<td><a href='school_master.php?action=edit&school_id=" . $row['school_id'] . "'><i class='fa fa-edit'></i></a><a href='javascript:void(0)' onclick='delete_record(" . $row['school_id'] . ")'>&nbsp;&nbsp;<i class='fa fa-trash'></i> </a></td>";
                            }
                            ?>

                            <?php
                        } else {
                            echo "<tr>No Record Found</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>


    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>

    <script type="text/javascript">

        $('.form_date').datetimepicker({
            language: 'en',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
        $(document).ready(function () {

            school_state = $("#school_state :selected").val();
            $.ajax({
                /* THEN THE AJAX CALL */
                type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                url: "school_master.php", /* PAGE WHERE WE WILL PASS THE DATA */
                data: {school_state_ajax: school_state}, /* THE DATA WE WILL BE PASSING */
                success: function (result) { /* GET THE TO BE RETURNED DATA */
                    result = JSON.parse(result);
                    var $select = $('#school_location');
                    $select.find('option').remove();
                    $select.append('<option value=' + "" + '>Select City</option>');
                    c_location = '<?php echo $school_location; ?>';
                    $.each(result, function (key, value) {
                        if (c_location == key)
                            $select.append('<option value=' + key + ' selected>' + value + '</option>');
                        else
                            $select.append('<option value=' + key + ' >' + value + '</option>');

                    });
                }
            });

            $("#sms_sent").css('display', 'none');
            $('#school_code').change(function () {
                school_code = $("#school_code").val();
                $.ajax({
                    /* THEN THE AJAX CALL */
                    type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "school_master.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {school_code_ajax: school_code}, /* THE DATA WE WILL BE PASSING */
                    success: function (result) { /* GET THE TO BE RETURNED DATA */
                        result = JSON.parse(result);
                        $("#sms_sent").css('display', 'block');
                        $("#sms_sent").html(result)
                    }
                });
            });

            $('#school_state').change(function () {
                school_state = $("#school_state :selected").val();
                $.ajax({
                    /* THEN THE AJAX CALL */
                    type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "school_master.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {school_state_ajax: school_state}, /* THE DATA WE WILL BE PASSING */
                    success: function (result) { /* GET THE TO BE RETURNED DATA */
                        result = JSON.parse(result);
                        var $select = $('#school_location');
                        $select.find('option').remove();
                        $select.append('<option value=' + "" + '>Select City</option>');
                        $.each(result, function (key, value) {
                            $select.append('<option value=' + key + '>' + value + '</option>');
                        });
                    }
                });
            });

        });

        function delete_record(id) {
            var result = confirm("Are you sure to delete?");
            if (result) {
                $.ajax({
                    /* THEN THE AJAX CALL */
                    type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "school_master.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {action: 'delete', 'school_id': id}, /* THE DATA WE WILL BE PASSING */
                    success: function (result) { /* GET THE TO BE RETURNED DATA */
                        location.reload();
                    }
                });
            }
        }
    </script>

    <?php include "footer.php" ?>
    </body>
    </html>

<?php
} else {
    header("Location: index.php"); /* Redirect browser */
    exit();
}