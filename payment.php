<?php
/**
 * Created by PhpStorm.
 * User: Aakash
 * Date: 04-10-2017
 * Time: 12:20
 */
session_start();
include "connection.php";
require 'FlashMessages.php';
$alert_msg = new \Plasticbrain\FlashMessages\FlashMessages();
if (isset($_GET['school_id_ajax'])) {
    $query = "select pi_no,id from invoice where status=1 AND school_id =" . $_GET['school_id_ajax'] . "";
    $result = mysqli_query($connect, $query);
    $i = 1;
    $pi_no = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $pi_no[$row['id']] = $row['pi_no'];
        }

    }
    echo json_encode($pi_no);
    exit;
}

if (isset($_GET['balance_ajax'])) {
    $query = "select * from invoice where status=1 AND id ='" . $_GET['balance_ajax'] . "'";
    $result = mysqli_query($connect, $query);
    $amount_to_pay_ajax=0;
    if ($result->num_rows > 0) {
        $pi_duration='';
        $school_id='';
        while ($row = mysqli_fetch_array($result)) {
            $total = $row['total'];
            $cgst = $row['cgst'];
            $igst = $row['igst'];
            $sgst = $row['sgst'];
            $pi_duration =   $row['pi_duration'];
            $school_id =   $row['school_id'];
            $tax=$cgst+$igst+$sgst;
            $amount_to_pay_ajax=$total+$tax;
        }

        $query = "select amount_received,discount from payment where status=1 AND pi_id ='" . $_GET['balance_ajax'] . "'";
        $result = mysqli_query($connect, $query);
        $amount_received=0;
        $discount=0;
        if ($result->num_rows > 0) {
            while ($row_received = mysqli_fetch_array($result)) {
                $amount_received +=$row_received['amount_received'];
                $discount +=$row_received['discount'];
            }
        }
        $query_amt_pan = "select bounce_penalty from payment where status=1  AND pi_id IN (select id from invoice where pi_duration='".Date("M", strtotime($pi_duration . " last month"))."' AND school_id =  '" . $school_id . "')";
        $result_amount_pan = mysqli_query($connect, $query_amt_pan);
        $bounce_penalty=0;
        if ($result_amount_pan->num_rows > 0) {
            while ($panelty_amt = mysqli_fetch_array($result_amount_pan)) {
                $bounce_penalty +=$panelty_amt['bounce_penalty'];
            }
        }

        $amount_to_pay_ajax=$amount_to_pay_ajax-$amount_received;
        $amount_to_pay_ajax=$amount_to_pay_ajax-$discount;
        $amount_to_pay_ajax=$amount_to_pay_ajax+$bounce_penalty;
        $amount_to_pay_ajax=$amount_to_pay_ajax>=0?$amount_to_pay_ajax:0;
    }
    echo json_encode(round($amount_to_pay_ajax));
    exit;
}
if (isset($_SESSION['user']) && $_SESSION['user'] == 'admin') {
    $query = "select team_name,id from team_m where status=1";
    $result = mysqli_query($connect, $query);
    $i = 1;
    $team = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $team[$row['id']] = $row['team_name'];
        }
    }
                if (isset($_POST['team_id'])) {

                    $post_data = $_POST;
                    /* $school_id = !empty($post_data['school_id'])?$post_data['school_id']:'';
                     $team_id = !empty($post_data['team_id'])?$post_data['team_id']:'';*/
                    $pi_no = !empty($post_data['pi_no'])?$post_data['pi_no']:'';
                    $query = "select * from invoice where status=1 AND id ='" .$pi_no . "'";
                    $result = mysqli_query($connect, $query);
                    $amount_to_pay=0;
                    if ($result->num_rows > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            $pi_id= $row['id'];
                            $total = $row['total'];
                            $cgst = $row['cgst'];
                            $igst = $row['igst'];
                            $sgst = $row['sgst'];
                            $tax=$cgst+$igst+$sgst;
                            $amount_to_pay=$total+$tax;
                        }
                    }
                    $payment_medium = !empty($post_data['payment_medium'])?$post_data['payment_medium']:'';
                    $cheque_no=!empty($post_data['cheque_no'])?$post_data['cheque_no']:'';
                    $cheque_bank_name=!empty($post_data['cheque_bank_name'])?$post_data['cheque_bank_name']:'';
                    $cheque_date=!empty($post_data['cheque_date'])?date('Y-m-d',strtotime($post_data['cheque_date'])):date('Y-m-d');
                    $online_ref_no=!empty($post_data['online_ref_no'])?$post_data['online_ref_no']:'';
                    $discount = !empty($post_data['discount'])?$post_data['discount']:0;
                    $amount_received = !empty($post_data['amount_received'])?$post_data['amount_received']:0;
                    $created_on=date('Y-m-d h:s:i');
                    $query_amt_rec = "select amount_received,discount from payment where status=1 AND pi_id ='" . $pi_id. "'";
                    $result_amount_rec = mysqli_query($connect, $query_amt_rec);
                    $total_amount_received=0;
                    $discount_total=0;
                    if ($result_amount_rec->num_rows > 0) {
                        while ($amount_rec = mysqli_fetch_array($result_amount_rec)) {
                            $total_amount_received +=$amount_rec['amount_received'];
                            $discount_total +=$amount_rec['discount'];
                        }
                    }
                    $balanced=0;
                    $grand_total=$amount_to_pay;
                    $balanced=$grand_total-$total_amount_received;
                    $balanced=$balanced-$amount_received;
                    $balanced=$balanced-$discount_total;
                    $balanced=round($balanced-$discount);
                    $balanced=$balanced>0?$balanced:0;
                    $sql_statement = "INSERT INTO payment (pi_id, amount_received, payment_medium ,status,discount,balance,created_on,online_ref_no,cheque_no,cheque_bank_name,cheque_date) 
                                      VALUES ('" . $pi_id . "', '" . $amount_received . "','" .$payment_medium . "', '1','" . $discount . "','" . $balanced . "', '" . $created_on . "', '" . $online_ref_no . "', '" . $cheque_no . "', '" . $cheque_bank_name . "', '" . $cheque_date . "')";
                   // echo $sql_statement;die;
                    $flag = mysqli_query($connect, $sql_statement);
                    if ($flag) {
                        $alert_msg->success('Payment has been done successfully');

                    } else {
                        $alert_msg->error('There is an error during save');
                    }

                    // header("Location: payment.php"); /* Redirect browser */


                }

    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/mystyle.css" rel="stylesheet" media="screen">
        <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet" media="screen">
    </head>
    <body>
    <?php include "header.php"?>
    <div class="container-fluid h-100 col-md-12 float-left">
        <div class="row h-100">
            <?php include "left_menu.php"; ?>

            <div class="col pt-2 col-md-10">
                <?php if ($alert_msg->hasMessages($alert_msg::SUCCESS)) { ?>
                    <div class="alert-success"><?php echo $alert_msg->display(); ?></div>
                <?php } else{ ?>
                    <div class="alert-warning">
                        <?php echo $alert_msg->display(); ?>
                    </div>

                    <?php

                }?>
                <h2 class="form-signin-heading">Payment</h2>

                <form class="form-signin  mt-10" method="post">
                    <div class="col-md-12 mt-5">

                        <div class="form-group">
                            <select class="form-control  float-left col-md-3" name="team_id" required id="team_id"
                                    value="<?php echo !empty($_GET['team_id']) ? $_GET['team_id'] : '' ?>">
                                <option value="">Team Name</option>
                                <?php foreach ($team as $key => $t) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $t; ?></option>
                                <?php } ?>

                            </select>

                            <select class="form-control  float-left col-md-3  ml-5" id="schools" name="school_id"
                                              required value="<?php echo !empty($_GET['school_id']) ? $_GET['school_id'] : '' ?>">
                                <option value="">School Name</option>
                            </select>

                        </div>
                    </div>

                    <div class="col-md-12 mt-5">

                        <div class="form-group">
                            <select class="form-control  float-left col-md-3" id="pi_no" name="pi_no"
                                    required value="<?php echo !empty($_GET['school_id']) ? $_GET['school_id'] : '' ?>">
                                <option value="">Pi No</option>
                            </select>

                            <select class="form-control  float-left col-md-3  ml-5" id="payment_medium" name="payment_medium"  required >
                                <option value="">Payment Medium</option>
                                <option value="cash">Cash</option>
                                <option value="cheque">Cheque</option>
                                <option value="online">Online</option>
                            </select>
                            <input type="text" name="online_ref_no" placeholder="Online Ref No"  id="online_ref_no"
                                   class="form-control  float-left col-md-3  ml-5 other_medium online_ref_no" style="display: none;">
                        </div>
                    </div>
                    <div class="col-md-12 mt-5 cheque_info other_medium" style="display: none;" id="cheque_info">

                        <div class="form-group ">
                            <input type="text" name="cheque_no" placeholder="Cheque No" id="cheque_no"
                                   class="form-control  float-left col-md-3  ">
                            <input type="text" name="cheque_bank_name" placeholder="Cheque Bank Name"  id="cheque_bank_name"
                                   class="form-control  float-left col-md-3  ml-5">

                            <input type="text" name="cheque_date" placeholder="Cheque Date"  id="cheque_date"
                                   class="form-control  float-left col-md-3  ml-5">

                        </div>
                    </div>
                    <div class="col-md-12 mt-5">
                        <div class="form-group">
                            <input type="text" name="discount" placeholder="Discount if any" id="discount" autocomplete="off"
                                   class="form-control  float-left col-md-3">
                            <input type="text" name="amount_received" placeholder="Amount Paid" required id="paid_amount"
                                   class="form-control  float-left col-md-3  ml-5">
                        </div>
                    </div>

                    <div class="col-md-12 mt-5">
                        <div class="form-group">
                    <input type="text" name="balance" placeholder="Balance Amount" id="balance_amount"
                           class="form-control  float-left col-md-3" readonly>
                    <input type="submit" class="form-control btn btn-info col-md-3 ml-5" value="Payment">
                        </div>
                    </div>
                </form>
               <?php
                $query = "select *,P.created_on as created_on  from payment P,invoice I where P.status=1 AND P.pi_id=I.id ORDER BY balance ASC";
                $result = mysqli_query($connect, $query);
                ?>
                <div class="col-md-12">
                    <h3>Payment Details:</h3>
                    <table class="table" style=" font-size: 12px!important;">
                        <thead>
                        <tr>
                            <th>PI.No</th>
                            <th>Amt rec</th>
                            <th>Balance</th>
                            <th>Discount</th>
                            <th>Grand Total</th>
                            <th>PayMedium</th>
                            <th>Online Ref no</th>
                            <th>Cheque No</th>
                            <th>Cheque Bank</th>
                            <th>Penalty</th>
                            <th>Cleared</th>
                            <th>Chq Status</th>
                            <th>Chq Date</th>
                            <th>Created On</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($result->num_rows > 0) {
                            $data_saved = 0;
                            $grand_total=0;
                            $discount=0;

                            while ($row = mysqli_fetch_array($result)) {
                                /*   echo "<pre>";
                                   print_r($row);die;*/
                                $query_amt_rec = "select amount_received,discount from payment where status=1 AND pi_id ='" . $row['pi_id'] . "'";
                                $result_amount_rec = mysqli_query($connect, $query_amt_rec);
                                $amount_received=0;
                                if ($result->num_rows > 0) {
                                    while ($amount_rec = mysqli_fetch_array($result_amount_rec)) {
                                        $amount_received +=$amount_rec['amount_received'];
                                        $discount += $amount_rec['discount'];
                                    }
                                }
                                $query_amt_pan = "select bounce_penalty from payment where status=1  AND pi_id IN (select id from invoice where pi_duration='".Date("M", strtotime($row['pi_duration'] . " last month"))."' AND school_id =  '" . $row['school_id'] . "')";
                                $result_amount_pan = mysqli_query($connect, $query_amt_pan);

                                $bounce_penalty=0;
                                if ($result_amount_pan->num_rows > 0) {
                                    while ($panelty_amt = mysqli_fetch_array($result_amount_pan)) {
                                        $bounce_penalty +=$panelty_amt['bounce_penalty'];
                                    }
                                }
                                $tax = $row['cgst'] + $row['sgst'] + $row['igst'];
                                $total = $row['total'];
                                $grand_total=0;
                                $grand_total=$total + $tax;
                                $balanced = $grand_total - $amount_received;
                                $balanced = $balanced -$discount;
                                $balanced = $balanced + $bounce_penalty;
                                $balanced = $balanced > 0 ? $balanced : 0;
                                echo "<tr>";
                                $cheque_d='NA';
                                if($row['payment_medium']!='online')
                                    $row['online_ref_no']="NA";
                                if($row['payment_medium']!='cheque'){
                                    $row['cheque_bank_name']='NA';
                                    $row['cheque_no']='NA';
                                    $row['is_cleared_cheque'] ='NA';
                                    $row['cheque_status'] ='NA';

                                }else if($row['payment_medium']=='cheque'){
                                    $cheque_d=$row['cheque_date']!=''?date('d-m-Y',strtotime($row['cheque_date'])):'NotAvailable' ;
                                }
                            $balanced=    intVal($row['balance'])+ intVal($bounce_penalty);
                                echo "<td>" . $row['pi_no'] . "</td>";
                                echo "<td>" . $row['amount_received'] . "</td>";
                                echo "<td>" . $balanced . "</td>";
                                echo "<td>" . $row['discount'] . "</td>";
                                echo "<td><span style='font-size: 12px;font-weight: bold'>" . round($grand_total) . "</span></td>";
                                echo "<td>" . $row['payment_medium'] . "</td>";
                                echo "<td>" . $row['online_ref_no'] . "</td>";
                                echo "<td>" . $row['cheque_no'] . "</td>";
                                echo "<td>" . $row['cheque_bank_name'] . "</td>";
                                echo "<td>" . $bounce_penalty . "</td>";
                                echo "<td>" . $row['is_cleared_cheque'] . "</td>";
                                echo "<td>" . $row['cheque_status'] . "</td>";
                                echo "<td>" . $cheque_d. "</td>";
                                echo "<td>" . date('d-m-Y',strtotime($row['created_on'])) . "</td>";

                            }
                            ?>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
        <script>
            $(document).ready(function () {

                $('#cheque_date').datepicker();

                $('#team_id').change(function () {
                    team_id = $("#team_id :selected").val();
                    $.ajax({
                        /* THEN THE AJAX CALL */
                        type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                        url: "pigenerate.php", /* PAGE WHERE WE WILL PASS THE DATA */
                        data: {team_id_ajax: team_id}, /* THE DATA WE WILL BE PASSING */
                        success: function (result) { /* GET THE TO BE RETURNED DATA */
                            result = JSON.parse(result);
                            var $select = $('#schools');
                            $select.find('option').remove();
                            $select.append('<option value=' + "" + '>School Name</option>');
                            $.each(result, function (key, value) {
                                $select.append('<option value=' + key + '>' + value + '</option>');
                            });
                            /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
                        }
                    });
                });

                $('#team_id').change(function () {
                    team_id = $("#team_id :selected").val();
                    $.ajax({
                        /* THEN THE AJAX CALL */
                        type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                        url: "pigenerate.php", /* PAGE WHERE WE WILL PASS THE DATA */
                        data: {team_id_ajax: team_id}, /* THE DATA WE WILL BE PASSING */
                        success: function (result) { /* GET THE TO BE RETURNED DATA */
                            result = JSON.parse(result);
                            var $select = $('#schools');
                            $select.find('option').remove();
                            $select.append('<option value=' + "" + '>School Name</option>');
                            $.each(result, function (key, value) {
                                $select.append('<option value=' + key + '>' + value + '</option>');
                            });
                            /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
                        }
                    });
                });

                $('#schools').change(function () {
                    school_id = $("#schools :selected").val();
                    $.ajax({
                        /* THEN THE AJAX CALL */
                        type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                        url: "payment.php", /* PAGE WHERE WE WILL PASS THE DATA */
                        data: {school_id_ajax: school_id}, /* THE DATA WE WILL BE PASSING */
                        success: function (result) { /* GET THE TO BE RETURNED DATA */
                            result = JSON.parse(result);
                            var $select = $('#pi_no');
                            $select.find('option').remove();
                            $select.append('<option value=' + "" + '>Pi No</option>');
                            $.each(result, function (key, value) {
                                $select.append('<option value=' + key + '>' + value + '</option>');
                            });
                            /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
                        }
                    });
                });

                $('#pi_no').change(function () {
                    pi_no = $("#pi_no :selected").val();
                    $.ajax({
                        /* THEN THE AJAX CALL */
                        type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                        url: "payment.php", /* PAGE WHERE WE WILL PASS THE DATA */
                        data: {balance_ajax: pi_no}, /* THE DATA WE WILL BE PASSING */
                        success: function (result) { /* GET THE TO BE RETURNED DATA */
                            result = JSON.parse(result);
                           $("#balance_amount").val(result);
                            discount = $("#discount").val();
                            balance=$("#balance_amount").val();
                            balance=balance-discount;
                            balance=balance>0?balance:0;
                            $("#balance_amount").val(balance);
                        }
                    });
                });
                $('#payment_medium').change(function () {
                    payment_medium = $("#payment_medium :selected").val();
                   if(payment_medium=='cheque'){
                       $(".other_medium").hide();
                       $(".cheque_info").show();
                   }else if(payment_medium=="online"){
                       $(".other_medium").hide();
                       $(".online_ref_no").show();
                   }else{
                       $(".other_medium").hide();

                   }
                });
                $('#discount').keyup(function () {
                   keyupBalanceUpdate();
                });
                $('#discount').change(function () {
                   onchangeBalanceUpdate();

                });
                $('#discount').focusin(function () {
                    infocusBalanceUpdate();

                });

                $('#paid_amount').keyup(function () {
                   keyupBalanceUpdate();
                });
                $('#paid_amount').change(function () {
                   onchangeBalanceUpdate();


                });
                $('#paid_amount').focusin(function () {
                    infocusBalanceUpdate();
                });

            });
            function  keyupBalanceUpdate() {
                pi_no = $("#pi_no :selected").val();
                $.ajax({
                    /* THEN THE AJAX CALL */
                    type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "payment.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {balance_ajax: pi_no}, /* THE DATA WE WILL BE PASSING */
                    success: function (result) { /* GET THE TO BE RETURNED DATA */
                        result = JSON.parse(result);
                        $("#balance_amount").val(result);
                        discount = $("#discount").val();
                        balance=$("#balance_amount").val();
                        paid_amount=$("#paid_amount").val();
                        if(paid_amount==undefined)
                            paid_amount=0;
                        balance=balance-discount-paid_amount;
                        balance=balance>0?balance:0;
                        $("#balance_amount").val(balance);
                    }
                });
            }
            function infocusBalanceUpdate() {
                pi_no = $("#pi_no :selected").val();
                $.ajax({
                    /* THEN THE AJAX CALL */
                    type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "payment.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {balance_ajax: pi_no}, /* THE DATA WE WILL BE PASSING */
                    success: function (result) { /* GET THE TO BE RETURNED DATA */
                        result = JSON.parse(result);
                        balance=result;
                        paid_amount=$("#paid_amount").val();
                        if(paid_amount==undefined)
                            paid_amount=0;
                        balance=balance-discount-paid_amount;
                        $("#balance_amount").val(balance);


                    }
                });
            }

            function onchangeBalanceUpdate() {
                pi_no = $("#pi_no :selected").val();
                $.ajax({
                    /* THEN THE AJAX CALL */
                    type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "payment.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {balance_ajax: pi_no}, /* THE DATA WE WILL BE PASSING */
                    success: function (result) { /* GET THE TO BE RETURNED DATA */
                        result = JSON.parse(result);
                        $("#balance_amount").val(result);
                        discount = $("#discount").val();
                        balance=$("#balance_amount").val();
                        paid_amount=$("#paid_amount").val();
                        if(paid_amount==undefined)
                            paid_amount=0;
                        balance=balance-discount-paid_amount;
                        balance=balance>0?balance:0;
                        $("#balance_amount").val(balance);
                    }
                });
            }



        </script>
    </div>
        <?php include "footer.php"?>

    </body>
    </html>

    <?php
} else {
    header("Location: index.php"); /* Redirect browser */
    exit();
}