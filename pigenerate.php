<?php
/**
 * Created by PhpStorm.
 * User: Aakash
 * Date: 04-10-2017
 * Time: 12:20
 */
session_start();
include "connection.php";
require 'FlashMessages.php';

$msg_pi = new \Plasticbrain\FlashMessages\FlashMessages();

if (isset($_GET['team_id_ajax'])) {
    $query = "select school_name,id from school_m where status=1 AND team_id =" . $_GET['team_id_ajax'] . "";
    $result = mysqli_query($connect, $query);
    $school = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $school[$row['id']] = $row['school_name'];
        }

    }

    echo json_encode($school);
    exit;
}
if (isset($_SESSION['user']) && $_SESSION['user'] == 'admin') {
    $query = "select team_name,id from team_m where status=1";
    $result = mysqli_query($connect, $query);
    $i = 1;
    $team = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $team[$row['id']] = $row['team_name'];
        }
    }
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/mystyle.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
        <style>
            .table td, .table th {
                padding: 0.55rem !important;
            }
        </style>
    </head>
    <body>
    <?php include "header.php" ?>

    <div class="container-fluid h-100">
        <div class="row h-100">
            <?php include "left_menu.php"; ?>

            <div class="col pt-2 col-md-10">
                <h2 class="form-signin-heading">PI Generate</h2>
                <form class="form-signin  mt-10" method="post">
                    <div class="col-md-12 mt-5">

                        <div class="form-group">
                            <select class="form-control  float-left col-md-3" name="team_id" required id="team_id"
                                    value="<?php echo !empty($_GET['team_id']) ? $_GET['team_id'] : '' ?>">
                                <option value="">Team Name</option>
                                <?php foreach ($team as $key => $t) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $t; ?></option>
                                <?php } ?>

                            </select>

                            <select class="form-control  float-left col-md-3  ml-5" id="schools" name="school_id"
                                    required value="<?php echo !empty($_GET['school_id']) ? $_GET['school_id'] : '' ?>">
                                <option value="">School Name</option>
                            </select>
                            <input type="submit" class="form-control btn btn-info col-md-2 ml-3" value="Search">

                            <a class="form-control btn btn-info col-md-2 ml-3" href="pigenerate.php?action=generate_pi">Generate
                                PI </a>

                        </div>
                    </div>
                </form>
                <?php
                if (isset($_GET['action']) && $_GET['action'] == 'generate_pi') {

                    /* $post_data = $_POST;
                     $school_id = $post_data['school_id'];
                     $team_id = $post_data['team_id'];*/
                   // $last_month_start = date('Y-m-01', strtotime('last month'));
                    $last_month_end = date('Y-m-t', strtotime('last month'));
                    //$last_month_end = date('Y-m-t', strtotime('last month'));
                    $query_school = "select * from school_m S  where S.status=1";
                    $result_school = mysqli_query($connect, $query_school);
                    if ($result_school->num_rows > 0) {
                        $data_saved = 0;
                        $pi_generate = 0;
                        while ($schools = mysqli_fetch_array($result_school)) {
                            //echo $schools['bill_start_date'];die;
                            $bill_start_date = date('Y-m-d', strtotime($schools['bill_start_date']));
                            $bill_start_month = date('m', strtotime($schools['bill_start_date']));
                            $last_month_start_for_bill = date('m', strtotime('last month'));
                            if ($bill_start_date <= $last_month_end) {
                                $loop_flag = 0;
                                $bill_start_date = date('Y-m-d', strtotime($schools['bill_start_date']));
                                $bill_end_date = date('Y-m-t', strtotime($bill_start_date));

                                while ($loop_flag == 0) {
                                    $date1 = new DateTime( $bill_start_date);
                                    $date2 = new DateTime($bill_end_date);
                                    $days_diff = $date2->diff($date1)->format("%a");
                                    $days_diff++;
                                    //$pi_duration = date('M', strtotime($bill_start_date));
                                    $pi_duration = date('M', strtotime($bill_start_date));
                                    $pi_duration =  Date("M", strtotime($pi_duration . " next month"));
                                    $query = "select id from invoice where school_id=" . $schools['id'] . " AND pi_duration='" . $pi_duration . "'";
                                    $result = mysqli_query($connect, $query);

                                   /*Get last month sms*/
                                    $last_month_bill_start_date=date('Y-m-1',strtotime($bill_start_date.' last month'));
                                    $last_month_bill_end_date=date('Y-m-t',strtotime($last_month_bill_start_date));
                                    $last_sms_query = "select *,S.school_name as school_name,S.id as school_id from school_m S LEFT JOIN users_count UC ON S.school_code=UC.school_code where S.status=1 AND S.id='" . $schools['id'] . "' AND S.team_id='" . $schools['team_id'] . "' AND date(UC.created_date)>='" . $last_month_bill_start_date . "' AND date(UC.created_date)<='" . $last_month_bill_end_date . "'  order by date(created_date) DESC LIMIT  1";
                                    $last_sms_result = mysqli_query($connect, $last_sms_query);

                                     $last_month_sms=0;
                                    if ($last_sms_result->num_rows > 0) {
                                        while ($last_sms_row = mysqli_fetch_array($last_sms_result)) {
                                              $last_month_sms=$last_sms_row['total_sent_messages'];
                                        }
                                    }
                                          //echo $last_month_bill_start_date." ";

                                    if ($result->num_rows <= 0) {

                                        $query = "select *,S.school_name as school_name,S.id as school_id from school_m S LEFT JOIN users_count UC ON S.school_code=UC.school_code where S.status=1 AND S.id='" . $schools['id'] . "' AND S.team_id='" . $schools['team_id'] . "' AND date(UC.created_date)>='" . $bill_start_date . "' AND date(UC.created_date)<='" . $bill_end_date . "'  order by date(created_date) DESC LIMIT  1";

                                        $result = mysqli_query($connect, $query);
                                        if ($result->num_rows > 0) {
                                            $data_saved = 0;
                                            while ($row = mysqli_fetch_array($result)) {
                                                $security_count_query = "select id from invoice where school_id='" . $row['school_id'] . "'";
                                                $security_count_result = mysqli_query($connect, $security_count_query);
                                                if ($security_count_result->num_rows > 0) {
                                                    $row['security_amount'] = 0;
                                                }
                                                $tax = 0;
                                                $state = strtolower($row['school_state']);
                                                $sms_charges = ($row["total_sent_messages"] - $row["free_sms_count"]-$last_month_sms) * $row["sms_charging_rate"];
                                                if ($sms_charges < 0)
                                                    $sms_charges = 0;
                                                $pckg_rate = $row['package_rate'];
                                                $pckg_rate=($pckg_rate/$days_diff)*$days_diff;
                                              //  echo $pckg_rate;die;
                                                $total = ($row['student_count'] * $pckg_rate) + $sms_charges;
                                                $cgst = $sgst = $igst = 0;
                                                if ($state == 'up' || $state == 'uttar pradesh') {
                                                    $igst = $total * 18 / 100;  //UP=18%
                                                } else {
                                                    $cgst = $total * 9 / 100;  //Other=9%
                                                    $sgst = $total * 9 / 100;  //Other=9%
                                                }
                                                $total = $total + $row['security_amount'];
                                                $pi_query = "SELECT pi_no FROM `invoice` where school_id=(select id from school_m where school_code='" . $row['school_code'] . "') Order By id DESC LIMIT 1";
                                                $pi_result = mysqli_query($connect, $pi_query);
                                                $latest_pi = '';
                                                $current_session = date('Y');
                                                $next_session = date("Y", strtotime("+1 year"));
                                                $session = $current_session . '-' . $next_session;
                                                if ($pi_result->num_rows > 0) {
                                                    while ($pi_row = mysqli_fetch_array($pi_result)) {
                                                        $latest_pi = $pi_row['pi_no'];
                                                        $pi_arr = explode('/', $latest_pi);

                                                        $inc_val = $pi_arr[3];
                                                        $inc_val++;

                                                        $latest_pi = 'UES/' . $session . '/' . $row['school_code'] . '/' . $inc_val;
                                                    }
                                                } else {
                                                    $latest_pi = 'UES/' . $session . '/' . $row['school_code'] . '/' . '1';
                                                }

                                                $pi_no = $latest_pi;
                                                $sql_statement = "INSERT INTO invoice (pi_no, pi_duration, school_id, student_count, security_amount,sms_cost, cgst, sgst, igst, status,total,created_on,total_days) 
                                      VALUES ('" . $pi_no . "', '" . $pi_duration . "','" . $row['school_id'] . "', '" . $row['student_count'] . "', '" . $row['security_amount'] . "','" . $sms_charges . "', '" . $cgst . "', '" . $sgst . "'
                                      ,'" . $igst . "','1','" . $total . "','" . date('Y-m-d H:s:i') . "','".$days_diff."')";
                                                $flag = mysqli_query($connect, $sql_statement);
                                                if ($flag && $data_saved == 0) {
                                                    $data_saved = 0;
                                                    $pi_generate = 1;


                                                } else {
                                                    $data_saved = 1;
                                                    break;

                                                }
                                            }
                                        }
                                    }
                                    $bill_start_date=date('Y-m-d',strtotime( "+1 day", strtotime( $bill_end_date ) ));
                                    $bill_end_date = date('Y-m-t', strtotime($bill_start_date));

                                  if($bill_end_date>$last_month_end) {

                                      $loop_flag = 1;
                                  }

                                }

                                /* else {
                        echo "No Record Found";
                    }*/
                            }
                        }
                        if ($data_saved == 0 && $pi_generate == 1) {
                            $msg_pi->success('Pi Has Been Generated');
                        } else if ($data_saved == 0 && $pi_generate == 0) {
                            $msg_pi->error('There is no record found for any school codes Or PI already generated for this month');
                        } else {
                            $msg_pi->error('There is an error during save please regenerate PI');
                        }
                    }
                }
                // echo $query;die;
                // echo "<pre>";

                if (isset($_POST['team_id'])) {
                    $school_id = $_POST['school_id'];
                    $team_id = $_POST['team_id'];
                    $query = "select *,S.school_name as school_name,S.id as school_id from school_m S,invoice I where S.status=1 AND S.id=I.school_id AND S.id=" . $school_id . " AND S.team_id=" . $team_id;

                } else {
                    $query = "select *,S.school_name as school_name,S.id as school_id from school_m S,invoice I where S.status=1 AND S.id=I.school_id";
                }
                $result = mysqli_query($connect, $query);
                $i = 1;

                ?>
                <?php if ($msg_pi->hasMessages($msg_pi::SUCCESS)) { ?>
                    <div class="alert-success"><?php echo $msg_pi->display(); ?></div>
                <?php } else { ?>
                    <div class="alert-warning">
                        <?php echo $msg_pi->display(); ?>
                    </div>

                    <?php

                } ?>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>PI.No</th>
                            <th>PI Duration</th>
                            <th>School Name</th>
                            <th>State</th>
                            <th>Location</th>
                            <th>Agreement Date</th>
                            <th>Billing Start</th>
                            <th>Security</th>
                            <th>No Of Student</th>
                            <th>Rate</th>
                            <th>Total Days</th>
                            <th>Sms Charges</th>
                            <th>Grand Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($result->num_rows > 0) {
                            $data_saved = 0;
                            $grand_total = 0;
                            $discount = 0;
                            while ($row = mysqli_fetch_array($result)) {
                                $discount = 0;
                                /*   echo "<pre>";
                                   print_r($row);die;*/
                                $query_amt_rec = "select amount_received,discount from payment where status=1 AND pi_id ='" . $row['id'] . "'";
                                $result_amount_rec = mysqli_query($connect, $query_amt_rec);
                                $amount_received = 0;
                                if ($result_amount_rec->num_rows > 0) {
                                    while ($amount_rec = mysqli_fetch_array($result_amount_rec)) {
                                        $amount_received += $amount_rec['amount_received'];
                                        $discount += $amount_rec['discount'];
                                    }
                                }

                                $query_amt_pan = "select bounce_penalty from payment where status=1  AND pi_id IN (select id from invoice where pi_duration='" . Date("M", strtotime($row['pi_duration'] . " last month")) . "' AND school_id IN (select id from school_m where school_code=  '" . $row['school_code'] . "'))";
                                $result_amount_pan = mysqli_query($connect, $query_amt_pan);

                                $bounce_penalty = 0;
                                if ($result_amount_pan->num_rows > 0) {
                                    while ($panelty_amt = mysqli_fetch_array($result_amount_pan)) {
                                        $bounce_penalty += $panelty_amt['bounce_penalty'];
                                    }
                                }


                                $tax = $row['cgst'] + $row['sgst'] + $row['igst'];
                                $total = $row['total'];
                                $grand_total = 0;
                                $grand_total = $total + $tax;
                                $balanced = $grand_total - $amount_received;
                                $balanced = $balanced - $discount;
                                $balanced = $balanced + $bounce_penalty;
                                $balanced = $balanced > 0 ? $balanced : 0;
                                echo "<tr>";
                                echo "<td>" . $row['pi_no'] . "</td>";
                                echo "<td>" . $row['pi_duration'] . "</td>";
                                echo "<td>" . $row['school_name'] . "</td>";
                                echo "<td>" . $row['school_state'] . "</td>";
                                echo "<td>" . $row['school_location'] . "</td>";
                                echo "<td>" . $row['agreement_date'] . "</td>";
                                echo "<td>" . $row['bill_start_date'] . "</td>";
                                echo "<td>" . $row['security_amount'] . "</td>";
                                echo "<td>" . $row['student_count'] . "</td>";
                                echo "<td>" . $row['package_rate'] . "</td>";
                                echo "<td>" . $row['total_days'] . "</td>";
                                echo "<td>" . $row['sms_cost'] . "</td>";
                                echo "<td><span style='font-size: 12px;'>CGST:" . $row['cgst'] . "
                                           SGST:" . $row['sgst'] . "<br>
                                           IGST:" . $row['igst'] . "<br>
                                           Total:" . $row['total'] . "</span><br><span>
                                           Tax:" . $tax . "</span><br>
                                          <span style='font-size: 12px;font-weight: bold'> Gr.Total:" . round($grand_total) . "</span><br>
                                          <span style='font-size: 12px;'> Paid:" . round($amount_received) . "</span><br>
                                          <span style='font-size: 12px;'>Discount:" . round($discount) . "</span><br>
                                          <span style='font-size: 12px;'>Penalties:" . round($bounce_penalty) . "</span><br>
                                          <span style='font-size: 12px;font-weight: bold'> Balance:" . round($balanced) . "</span>
                                           </td>";

                            }
                            ?>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <script
                src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                crossorigin="anonymous"></script>
        <script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#team_id').change(function () {
                    team_id = $("#team_id :selected").val();
                    $.ajax({
                        /* THEN THE AJAX CALL */
                        type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                        url: "pigenerate.php", /* PAGE WHERE WE WILL PASS THE DATA */
                        data: {team_id_ajax: team_id}, /* THE DATA WE WILL BE PASSING */
                        success: function (result) { /* GET THE TO BE RETURNED DATA */
                            result = JSON.parse(result);
                            var $select = $('#schools');
                            $select.find('option').remove();
                            $select.append('<option value=' + "" + '>School Name</option>');
                            $.each(result, function (key, value) {
                                $select.append('<option value=' + key + '>' + value + '</option>');
                            });
                            /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
                        }
                    });
                });

                $('#team_id').change(function () {
                    team_id = $("#team_id :selected").val();
                    $.ajax({
                        /* THEN THE AJAX CALL */
                        type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                        url: "pigenerate.php", /* PAGE WHERE WE WILL PASS THE DATA */
                        data: {team_id_ajax: team_id}, /* THE DATA WE WILL BE PASSING */
                        success: function (result) { /* GET THE TO BE RETURNED DATA */
                            result = JSON.parse(result);
                            var $select = $('#schools');
                            $select.find('option').remove();
                            $select.append('<option value=' + "" + '>School Name</option>');
                            $.each(result, function (key, value) {
                                $select.append('<option value=' + key + '>' + value + '</option>');
                            });
                            /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
                        }
                    });
                });
            });
        </script>
    </div>
    <?php include "footer.php" ?>

    </body>
    </html>

    <?php
} else {
    header("Location: index.php"); /* Redirect browser */
    exit();
}