<?php
/**
 * Created by PhpStorm.
 * User: Aakash
 * Date: 04-10-2017
 * Time: 12:20
 */
session_start();
include "connection.php";
require 'FlashMessages.php';
$alert_msg = new \Plasticbrain\FlashMessages\FlashMessages();

if (isset($_SESSION['user']) && $_SESSION['user'] == 'admin') {
    $flag=0;
    if (isset($_POST['submit_data'])) {

        $post_data = $_POST;
    $data_arr=array();
        /*echo "<pre>";
        print_r($post_data);exit;*/
       foreach ($post_data as $key=>$data){
           if($key!='submit_data') {

               $id=explode('-',$key);
               $payid = $id[1];
               $paycol = $id[0];
               $data_arr[$payid][$paycol] = $data;

               /*$sql_statement = "UPDATE payment SET school_name='" . $school_name . "', school_code= '" . $school_code . "', school_state='" . $school_state . "', school_location ='" . $school_location . "', team_id='" . $team_id . "',package_type='" . $package_type . "', free_sms_count='" . $free_sms_count . "', sms_charging_rate='" . $sms_charging_rate . "'
                , bill_start_date='" . $billing_start_date . "', package_rate='" . $payment_rate . "',agreement_date='" . $agreement_date . "',
                is_agreement='" . $is_agreement . "',security_amount='" . $security . "',status='" . $status . "',created_on='" . $created_date . "'";
               $flag = mysqli_query($connect, $sql_statement);
               //echo $sql_statement;die;
               if ($flag) {
                   $msg->success('School Has Been updated Successfully');

               } else {
                   $msg->error('There is an error during save');

               }*/
           }
       }
       /*echo "<pre>";
       print_r($data_arr);exit;*/
       foreach ($data_arr as $key=>$arr_data){
             $is_reconciled=0;
             $is_cleared=0;
             $penalty=0;
           $school_id=0;
           if(isset($arr_data['is_reconciled']) && $arr_data['is_reconciled']=='on'){
               $is_reconciled=1;
           } if(isset($arr_data['is_cleared_cheque']) && $arr_data['is_cleared_cheque']=='on'){
               $is_cleared=1;
           }if(isset($arr_data['bounce_penalty']) && $arr_data['bounce_penalty']!=' '){
               $penalty=$arr_data['bounce_penalty'];
           }if(isset($arr_data['pi_id']) && $arr_data['pi_id']!=' ') {
               $pi_id = $arr_data['pi_id'];
           }

           if($is_cleared==1 && $is_reconciled==1) {
               $sql_statement = "UPDATE payment SET is_cleared_cheque='" . $is_cleared . "', bounce_penalty= '" . $penalty . "', is_reconciled='" . $is_reconciled . "' ,cheque_status='cleared' where id='" . $key . "'";
               $flag = mysqli_query($connect, $sql_statement);

           }elseif($is_cleared==0 && $is_reconciled==1){

               /*==================*/

               /*$invoice_details = "select * from invoice where status=1 AND id ='" . $pi_id . "'";
              // echo $invoice_details;die;
               $invoice_data = mysqli_query($connect, $invoice_details);*/
               $amount_received=0;
               $discount=0;
             //  $invoice_data_pointer='';
/*
               if ($invoice_data->num_rows > 0) {
                   while ($invoice_data_temp= mysqli_fetch_array($invoice_data)) {

                       $invoice_data_pointer=$invoice_data_temp;
                   }
               }*/

               $query_amt_rec = "select amount_received,discount,balance from payment where status=1 AND id ='" . $key . "'";
               $result_amount_rec = mysqli_query($connect, $query_amt_rec);
               $amount_received=0;
               $discount=0;
               $submit_balance=0;
               if ($result_amount_rec->num_rows > 0) {
                   while ($amount_rec = mysqli_fetch_array($result_amount_rec)) {
                       $amount_received +=$amount_rec['amount_received'];
                       $discount += $amount_rec['discount'];
                       $submit_balance += $amount_rec['balance'];
                   }
               }
               /*$query_amt_pan = "select bounce_penalty from payment where status=1  AND pi_id IN (select id from invoice where pi_duration='".Date("M", strtotime($invoice_data_pointer['pi_duration'] . " last month"))."' AND school_id =  '" . $invoice_data_pointer['school_id'] . "')";
               $result_amount_pan = mysqli_query($connect, $query_amt_pan);

               $bounce_penalty=0;
               if ($result_amount_pan->num_rows > 0) {
                   while ($panelty_amt = mysqli_fetch_array($result_amount_pan)) {
                       $bounce_penalty +=$panelty_amt['bounce_penalty'];
                   }
               }
               $tax = $invoice_data_pointer['cgst'] + $invoice_data_pointer['sgst'] + $invoice_data_pointer['igst'];
               $total = $invoice_data_pointer['total'];
               $grand_total=0;
               $grand_total=$total + $tax;
               $balanced = $grand_total - $amount_received;
               $balanced = $balanced -$discount;
               $balanced = $balanced + $bounce_penalty;
               $balanced = $balanced > 0 ? round($balanced) : 0;*/
               $balanced=$amount_received+$discount+$submit_balance;
               //print_r($balanced);die;

               $sql_statement = "UPDATE payment SET balance='" . $balanced . "' where id='" . $key . "'";
               $flag = mysqli_query($connect, $sql_statement);

               $sql_statement = "UPDATE payment SET is_cleared_cheque='" . $is_cleared . "', bounce_penalty= '" . $penalty . "', is_reconciled='" . $is_reconciled . "',amount_received='" . 0 . "',discount='" . 0 . "',cheque_status='revert' where id='" . $key . "'";
               $flag = mysqli_query($connect, $sql_statement);

           }


           /*=============*/

           /*$sql_statement = "UPDATE payment SET is_cleared_cheque='" . $is_cleared . "', bounce_penalty= '" . $penalty . "', is_reconciled='" . $is_reconciled . "' where id='".$key."'";
           $flag = mysqli_query($connect, $sql_statement);*/


               //echo $sql_statement;die;
       }
        if ($flag) {
            $alert_msg->success('Cheque status changed successfully');

        } else {
            $alert_msg->error('There is an error during save');

        }

    }

    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/mystyle.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    </head>
    <body>
    <?php include "header.php"?>

    <div class="container-fluid h-100">
        <div class="row h-100">
            <?php include "left_menu.php"; ?>

            <div class="col pt-2 col-md-10">
                <?php if ($alert_msg->hasMessages($alert_msg::SUCCESS)) { ?>
                    <div class="alert-success"><?php echo $alert_msg->display(); ?></div>
                <?php } else{ ?>
                    <div class="alert-warning">
                        <?php echo $alert_msg->display(); ?>
                    </div>

                    <?php

                }?>
                <h2 class="form-signin-heading">Cheque Reconsile</h2>

                <form class="form-signin  mt-10" method="post">
                    <?php

                    $query = "select *,P.created_on as created_on,P.id as id  from payment P,invoice I where P.status=1 AND P.pi_id=I.id AND P.is_reconciled='0' AND P.payment_medium='cheque' ORDER BY P.created_on DESC";

                    $result = mysqli_query($connect, $query);
                    ?>
                    <input name="submit_data" value="" type="hidden">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>PI.No</th>
                                <th>Amount received</th>
                                <th>Balance</th>
                                <th>Cheque No</th>
                                <th>Cheque Bank</th>
                                <th>Created On</th>
                                <th>Is Cleared</th>
                                <th>Is Reconciled</th>
                                <th>Bounce Penalty</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($result->num_rows > 0) {
                                $data_saved = 0;
                                $grand_total=0;
                                $discount=0;
                                while ($row = mysqli_fetch_array($result)) {
                                    /*   echo "<pre>";
                                       print_r($row);die;*/
                                    $query_amt_rec = "select amount_received,discount,pi_id from payment P where P.status=1 AND pi_id ='" . $row['pi_id'] . "'";
                                    $result_amount_rec = mysqli_query($connect, $query_amt_rec);
                                    $amount_received = 0;
                                    if ($result_amount_rec->num_rows > 0) {
                                        while ($amount_rec = mysqli_fetch_array($result_amount_rec)) {
                                            $amount_received += $amount_rec['amount_received'];
                                        }
                                    }
                                    /*   $tax = $row['cgst'] + $row['sgst'] + $row['igst'];
                                       $total = $row['total'];
                                       $grand_total=0;
                                       $grand_total=$total + $tax;*/
                                    echo "<tr>";
                                    echo "<td>" . $row['pi_no'] . "</td>";
                                    echo "<td>" . $row['amount_received'] . "</td>";
                                    echo "<td>" . $row['balance'] . "</td>";
                                    echo "<td>" . $row['cheque_no'] . "</td>";
                                    echo "<td>" . $row['cheque_bank_name'] . "</td>";
                                    echo "<td>" . date('d-m-Y', strtotime($row['created_on'])) . "</td>";
                                    if ($row['is_cleared_cheque'] == 1)
                                        $checked = 'checked';
                                    else
                                        $checked = '';
                                    echo "<td><input type='checkbox' name='is_cleared_cheque-" . $row['id'] . "' " . $row['is_cleared_cheque'] . " " . $checked . "></td>";
                                    echo "<td><input type='checkbox' name='is_reconciled-" . $row['id'] . "'></td>";
                                    echo "<td><input type='text' name='bounce_penalty-" . $row['id'] . "' ></td>";
                                    echo "<td><input type='hidden' name='pi_id-" . $row['id'] . "'  value='". $row['pi_id']."'></td>";
                                }
                                ?>

                                <?php
                            }else{
                                echo "No cheque found";
                            }
                            ?>
                            </tbody>
                        </table>
                        <button type="submit" value="" class="form-control btn btn-info col-md-2 ml-5 float-right">SUBMIT</button>
                    </div>
                </form>

            </div>
        </div>
        <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
        <script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
       
    </div>
        <?php include "footer.php"?>

    </body>
    </html>

    <?php
} else {
    header("Location: index.php"); /* Redirect browser */
    exit();
}