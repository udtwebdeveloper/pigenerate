<?php
/**
 * Created by PhpStorm.
 * User: Aakash
 * Date: 04-10-2017
 * Time: 12:20
 */
session_start();
include "connection.php";
require 'FlashMessages.php';
$alert_msg = new \Plasticbrain\FlashMessages\FlashMessages();

if (isset($_GET['team_id_ajax'])) {
    $team_ids = $_GET['team_id_ajax'];
    $team_ids = implode(',', $team_ids);
    $query = "select school_name,id from school_m where status=1 AND team_id IN (" . $team_ids . ")";
    $result = mysqli_query($connect, $query);
    $school = array();
    $options = '';
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
//            $school[$row['id']] = $row['school_name'];
            $options .= '<option value=' . $row['id'] . '>' . ucfirst($row['school_name']) . '</option>';

        }

    }

    echo $options;
    exit;
}

if (isset($_SESSION['user']) && $_SESSION['user'] == 'admin') {
    $query = "select team_name,id from team_m where status=1";
    $result = mysqli_query($connect, $query);
    $i = 1;
    $team = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $team[$row['id']] = $row['team_name'];
        }
    }
    $payment_data = [];
    $payment_data = [];
    if (!empty($_GET['school_id'])) {
        $school_ids = $_GET['school_id'];

        $school_ids = implode(",", $school_ids);
        $query = "SELECT S.id as school_id,T.id as team_id,T.team_name,S.school_name,I.pi_no,I.pi_duration ,sum(P.amount_received) as total_received,I.total as total_amount,I.cgst  as cgst,I.igst  as igst,I.sgst as sgst,sum(P.discount)  as discount FROM `invoice` as I left join payment as P on P.pi_id=I.id left join school_m as S on S.id=I.school_id left join team_m as T on T.id= S.team_id where S.id IN(" . $school_ids . ")  group by I.pi_no ";
        $result = mysqli_query($connect, $query);
        if ($result->num_rows > 0) {
            $i = 0;
            while ($row = mysqli_fetch_array($result)) {

                $payment_data[$row['team_id']][$row['school_id']]['school_info']['team_name'] = $row['team_name'];
                $payment_data[$row['team_id']][$row['school_id']]['school_info']['school_name'] = $row['school_name'];
                $payment_data[$row['team_id']][$row['school_id']]['payment'][$row['pi_duration']]['pi_no'] = $row['pi_no'];
                $payment_data[$row['team_id']][$row['school_id']]['payment'][$row['pi_duration']]['pi_duration'] = $row['pi_duration'];
                $total_received = $row['total_received'];
                $payment_data[$row['team_id']][$row['school_id']]['payment'][$row['pi_duration']]['total_received'] = $total_received;
                $total_amt = $row['total_amount'] + $row['cgst'] + $row['igst'] + $row['sgst'];
                $payment_data[$row['team_id']][$row['school_id']]['payment'][$row['pi_duration']]['total_amount'] = $total_amt;
                $payment_data[$row['team_id']][$row['school_id']]['payment'][$row['pi_duration']]['discount'] = $row['discount'];
                $payment_data[$row['team_id']][$row['school_id']]['payment'][$row['pi_duration']]['school_id'] = $row['school_id'];
                $payment_data[$row['team_id']][$row['school_id']]['payment'][$row['pi_duration']]['balance'] = round($total_amt - $total_received - $row['discount']);

            }

            foreach ($payment_data as $team_id => $payments) {
                foreach ($payments as $school_id => $payment) {

                    $payment_details = $payment['payment'];
                    $bounce_penalty = 0;
                    foreach ($payment_details as $p_details) {
                        $pi_duration = $p_details['pi_duration'];
                        $query_amt_pan = "select bounce_penalty from payment where status=1  AND pi_id = (select id from invoice where pi_duration='" . Date("M", strtotime($pi_duration . " last month")) . "' AND school_id =  '" . $school_id . "')";
                        $result_amount_pan = mysqli_query($connect, $query_amt_pan);
                        if ($result_amount_pan->num_rows > 0) {
                            while ($panelty_amt = mysqli_fetch_array($result_amount_pan)) {
                                $bounce_penalty += $panelty_amt['bounce_penalty'];

                            }
                        }
                        $payment_data[$team_id][$school_id]['payment'][$pi_duration]['penalty'] = $bounce_penalty;

                    }

                }
            }

        }

    }

    ?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="./css/mystyle.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css"
          rel="stylesheet" media="screen">
    <link href="./bootstrap/css/bootstrap-multiselect.css" rel="stylesheet" media="screen">
</head>
<body>
    <?php include "header.php"?>
<div class="container-fluid h-100 col-md-12 float-left">
    <div class="row h-100">
        <?php include "left_menu.php"; ?>

        <div class="col pt-2 col-md-10">
            <?php if ($alert_msg->hasMessages($alert_msg::SUCCESS)) { ?>
            <div class="alert-success"><?php echo $alert_msg->display(); ?></div>
            <?php } else { ?>
            <div class="alert-warning">
                <?php echo $alert_msg->display(); ?>
            </div>

            <?php

        }?>
            <h2 class="form-signin-heading">Payment Details</h2>

            <form class="form-signin  mt-10" method="get">
                <div class="col-md-12 mt-5">
                    <div class="form-group">
                        <div class="col-md-12 mt-5">

                            <select class="form-control  float-left col-md-3" name="team_id" required id="team_id"
                                    multiple='multiple'
                                    value="<?php echo !empty($_GET['team_id']) ? $_GET['team_id'] : '' ?>">
                                <?php

                                foreach ($team as $key => $t) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $t; ?></option>
                                    <?php } ?>

                            </select>

                            <select class="form-control  float-left col-md-3  ml-5" id="schools" name="school_id[]"
                                    multiple='multiple'
                                    required value="<?php echo !empty($_GET['school_id']) ? $_GET['school_id'] : '' ?>">
                                <option value="">School Name</option>
                            </select>

                            <input type="submit" class="form-control btn btn-info col-md-3 ml-5" value="Search">

                        </div>

                    </div>
                </div>


                <div class="col-md-12 mt-5">
                    <div class="form-group">

                    </div>
                </div>
            </form>
            <?php
            if (!empty($payment_data)) {

                $Months = [];
                $MonthNumbers = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2];
                foreach ($MonthNumbers as $MonthNumber) {
                    $Months[] = date("M", mktime(0, 0, 0, $MonthNumber));
                }

                ?>
                <div class="col-md-12">
                    <!--<h3>Payment Details:</h3>-->
                    <table class="table" style=" font-size: 12px!important;">
                        <thead>
                        <tr>
                            <th>Team Name</th>
                            <th>School Name</th>
                            <?php foreach ($Months as $month) { ?>
                            <th><?php echo $month ?></th>
                            <?php } ?>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            $month_total=[];
                                foreach ($payment_data as $p_data) {
                                foreach ($p_data as $data) {
                                    $school_teams = !empty($data['school_info']) ? $data['school_info'] : [];
                                    echo "<tr><td>" . $school_teams['team_name'] . "</td>";
                                    echo "<td>" . $school_teams['school_name'] . "</td>";
                                    $payments = !empty($data['payment']) ? $data['payment'] : [];
                                    if (!empty($payments)) {
                                        $total_paid_amt=0;
                                        $total_unpaid_amt=0;
                                        foreach($Months as $pay_month){
                                          if(array_key_exists($pay_month,$payments)){
                                            $payment=$payments[$pay_month];
                                            $total_amt=!empty($payment['total_amount'])?$payment['total_amount']:0;
                                            $total_paid=!empty($payment['total_received'])?$payment['total_received']:0;
                                            $total_pnlty =!empty($payment['penalty'])?$payment['penalty']:0;
                                            $total_dis =!empty($payment['discount'])?$payment['discount']:0;
                                            $balance =!empty($payment['balance'])?$payment['balance']:0;
                                             $total_amt=$total_amt+$total_pnlty-$total_dis;
                                            $total_unpaid= $total_amt-$total_paid;
                                            echo '<td>Total:'.round($total_amt).'<div class="clearfix"></div>Paid:'.round($total_paid).'<div class="clearfix"></div>Unpaid:'.round($total_unpaid).'</td>';
                                            $total_paid_amt +=$total_paid;
                                            $total_unpaid_amt +=$total_unpaid;
                                       }else{
                                            echo '<td>Pi not generated</td>';
                                        }
                                     }

                                        echo '<td>Paid:'.round($total_paid_amt).'<div class="clearfix"></div>Unpaid:'.round($total_unpaid_amt).'</td>';
                                        echo '</tr>';
                                    }

                                }

                            }
                            echo '<tr>';
                            echo '<td><b>Total: </b></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '<td></td>';
                            echo '</tr>';
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php } ?>
        </div>
    </div>

    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./bootstrap/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
    <script>
        $(document).ready(function () {

            $('#team_id').multiselect({
                includeSelectAllOption:true
            });
            $('#schools').multiselect({
                includeSelectAllOption:true
            });

            $('#team_id').change(function () {
                team_id = $("#team_id").val();
                console.log(team_id);
                $.ajax({
                    type:"GET",
                    url:"payment_report.php",
                    data:{team_id_ajax:team_id},
                    success:function (result) {
                        /*result = JSON.parse(result);
                     var $select = $('#schools');*/
                        $("#schools").empty().append(result);
                        /*$.each(result, function (key, value) {
                            $select.append('<option value=' + key + '>' + value + '</option>');

                        });*/
                        $('#schools').multiselect('rebuild');

                    }
                });
            });

        });


    </script>
</div>
    <?php include "footer.php"?>

</body>
</html>

<?php
} else {
    header("Location: index.php"); /* Redirect browser */
    exit();
}